import java.util.Iterator;

public class Stack<T> implements Iterable<T>{
    private Node top;
    private int n;

    private class Node{
	T item;
	Node next;
	Node(T item, Node next){
	    this.item = item;
	    this.next = next;
	}
    }
    
    Stack(){
	top = null;
	n = 0;
    }

    void push(T data){
	Node newTop = new Node(data, top);
	top = newTop;
	n++;
    }

    T pop(){
	T data = top.item;
	top = top.next;
	n--;
	return data;
    }

    public boolean isEmpty(){
	return top == null;
    }

    public int size(){
	return n;
    }

    public Iterator<T> iterator(){
	return new ListIterator();
    }

    private class ListIterator implements Iterator<T>{
	private Node current = top;
	public boolean hasNext(){
	    return current != null;
	}
	public T next(){
	    T item = current.item;
	    current = current.next;
	    return item;
	}
    }
}

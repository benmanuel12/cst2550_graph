public class Graph{

    private final int V;
    private int E;
    private Queue<Integer>[] adj;

    public Graph(int V){
	this.V = V;
	adj = (Queue<Integer>[]) new Queue[V];
	for (int v = 0; v < V; ++v){
	    adj[v] = new Queue<Integer>();
	}
    }

    public int V(){return V;}
    public int E(){return E;}

    public void addEdge(int v, int w){
	adj[v].enqueue(w);
	adj[w].enqueue(v);
	E++;
    }

    public Iterable<Integer> adj(int v){
	return adj[v];
    }
}
